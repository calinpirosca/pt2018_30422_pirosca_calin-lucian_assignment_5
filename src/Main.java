import IO.FileIn;
/**
 * 
 * Main clased used to start the application and handle any thrown error
 * 
 * @author Calin Pirosca Lucian
 *
 */
public class Main {

	public static void main(String[] args) {
		try {
			FileIn.getActivities();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
