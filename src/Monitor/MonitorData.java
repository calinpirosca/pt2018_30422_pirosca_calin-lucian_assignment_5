package Monitor;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
/**
 * 
 * Blueprint class used to individually (theoretically) store each activity
 * 
 * @author Calin Pirosca Lucian
 *
 */
public class MonitorData {
	private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd kk:mm:ss");
	public LocalDateTime startDate, endDate;
	public String activity;
	//used to count the number of activities for each activity
	private static Map<String, Integer> activityCounter = new HashMap<String, Integer>();
	//used to count the duration for each activity
	private static Map<String, Duration> activityTimeCounter = new HashMap<String, Duration>();
	//used to count the activity count per day
	private static Map<Integer, Map<String, Integer>> dailyActivityCounter = new HashMap<Integer, Map<String, Integer>>();
	//used to record for each activity the duration for each day.
	private static Map<String, List<Duration>> activityTimerSample = new HashMap<String, List<Duration>>();
	//used to differentiate between two days.
	private static LocalDateTime lastStartDate = LocalDateTime.now();
	private static int currentDay = 0;

	/**
	 * Getter for startDate
	 * 
	 * @return the start date
	 */
	public LocalDateTime getStartDate() {
		return startDate;
	}
	/**
	 * 
	 * Getter for the end date
	 * 
	 * @return the end date
	 */
	public LocalDateTime getEndDate() {
		return endDate;
	}
	/**
	 * 
	 * Getter for activity
	 * 
	 * @return the activity
	 */
	public String getActivity() {
		return activity;
	}

	/**
	 * Constructor of the blueprint
	 * 
	 * 
	 * @param startDate when does the activity start
	 * @param endDate when does the activity end
	 * @param activity the activity
	 */
	public MonitorData(String startDate, String endDate, String activity) {
		this.startDate = LocalDateTime.parse(startDate, dtf);
		if (!(this.startDate.getDayOfYear() == lastStartDate.getDayOfYear())) {
			lastStartDate = this.startDate;
			currentDay++;
		}
		this.endDate = LocalDateTime.parse(endDate, dtf);
		this.activity = activity;
		logActivity(activity, this.startDate, this.endDate);
		logDailyActivity(startDate, activity);
	}

	/**
	 * 
	 * Log all the activites and their date and store them in a map to be later processed or to be returned when requested
	 * 
	 * @param activity the activity to be logged
	 * @param startDate when does it start
	 * @param endDate when does it end
	 */
	private static void logActivity(String activity, LocalDateTime startDate, LocalDateTime endDate) {
		//activityCounter.put(activity, (activityCounter.get(activity) == null ? 1 : activityCounter.get(activity) + 1));
		
		/*activityTimeCounter.put(activity, (activityTimeCounter.get(activity) == null ? getTime(startDate,endDate)
				: addTime(activityTimeCounter.get(activity), getTime(startDate,endDate))));
		*/
		activityCounter.computeIfAbsent(activity, v -> 0);
		activityCounter.computeIfPresent(activity, (k,v) -> v = v+1);
		
		activityTimeCounter.computeIfAbsent(activity, v -> Duration.ofMillis(0));
		activityTimeCounter.computeIfPresent(activity, (k,v) -> addTime(activityTimeCounter.get(activity), getTime(startDate, endDate)));
		
		activityTimerSample.computeIfAbsent(activity.trim(), k -> new LinkedList<>()).add(getTime(startDate, endDate));
	}

	/**
	 * 
	 * Helper method, used to return the duration between two dates
	 * 
	 * @param startDate the beginning of the period
	 * @param endDate the end of the period
	 * @return the difference between the startdate and enddate
	 */
	private static Duration getTime(LocalDateTime startDate, LocalDateTime endDate) {
		Duration duration = Duration.between(startDate, endDate);
		return duration;
		
	}

	/**
	 * 
	 * Adds 2 durations
	 * 
	 * @param date1 One duration
	 * @param date2 The other duration
	 * @return The sum of the 2 parameters
	 */
	private static Duration addTime(Duration date1, Duration date2) {
		return date1.plus(date2);
	}

	//HERE MODIFY compute!!
	/**
	 * 
	 * Log all the activites and their date and store them in a map to be later processed or to be returned when requested
	 * However, unlike LogActivity what this method does is to log each activity per day, that is log all the activites per day and 
	 * not overall as the other method does.
	 * 
	 * @param activity the activity to be logged
	 * @param startDate when does it start
	 */
	private static void logDailyActivity(String startDate, String activity) {
		if (dailyActivityCounter.get(currentDay) == null) {
			Map<String, Integer> activities = new HashMap<String, Integer>();
			activities.put(activity, 1);
			dailyActivityCounter.put(currentDay, activities);
		} else {
			Map<String, Integer> activities = dailyActivityCounter.get(currentDay);
			activities.put(activity, (activities.get(activity) == null ? 1 : activities.get(activity) + 1));
			dailyActivityCounter.replace(currentDay, activities);
		}
	}

	/**
	 * 
	 * Method used to return the number of days that were observed
	 * 
	 * @param input list of activities extracted from the file
	 * @return the number of days observed
	 */
	public static int getNumberOfDays(List<MonitorData> input) {
		return input.get(input.size() - 1).getEndDate().getDayOfYear() - input.get(0).getStartDate().getDayOfYear() + 1;
	}

	/**
	 * 
	 * @return a map used to count the number of activities for each activity
	 */
	public static Map<String, Integer> getActivityCounter() {
		return activityCounter;
	}

	/**
	 * 
	 * 
	 * @return a map used to count for each day the number of activites for each activity
	 */
	public static Map<Integer, Map<String, Integer>> getDailyActivityCounter() {
		return dailyActivityCounter;
	}
	
	/**
	 * 
	 * @return the overall duration of all the activities
	 */
	public static Map<String, Duration> getActivityTimeCounter(){
		return activityTimeCounter;
	}
	/**
	 * 
	 * 
	 * @return duration for each sample of each activity recorded.
	 */
	public static Map<String, List<Duration>> getActivityTimeSample(){
		return activityTimerSample;
	}
}
