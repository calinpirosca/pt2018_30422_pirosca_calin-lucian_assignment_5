package IO;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import Monitor.MonitorData;
/**
 * 
 * 
 * Class used to handle the file input
 * 
 * @author Calin Pirosca Lucian
 *
 */
public class FileIn {
	private static final String filePath = "C:\\Users\\Work\\Desktop\\Activities.txt";
	/**
	 * 
	 * Class used to retrieve all the activites from the specified file using a stream and then send the stream further 
	 * in order to be processed
	 * 
	 * @throws IOException if no file is found or is invalid
	 */
	public static void getActivities() throws IOException {
		Stream<String> stream = Files.lines(Paths.get(filePath));
		processData(stream);
		stream.close();
	}
	/**
	 * 
	 * Used to process the data extracted from the file and store them using the MonitorData blueprint
	 * 
	 * @param data the data extracted from the file
	 */
	public static void processData(Stream<String> data) {
		List<MonitorData> activities = new LinkedList<>();
		data.forEach(item->{
			String temp[] = item.split("\t\t");
			activities.add(new MonitorData(temp[0], temp[1], temp[2]));
		});
		FileOut.Output(activities);
	}
}
