package IO;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import Monitor.MonitorData;
/**
 * 
 * Class used to handle output operation, containing the predefined operations requested by the 'client'
 * 
 * @author Calin Pirosca Lucian
 *
 */
public class FileOut {
	public static Map<String, Integer> activityCounter;
	public static Map<String, Duration> activityTimeCounter = MonitorData.getActivityTimeCounter();
	public static int k,n;
	/**
	 * 
	 * 
	 * Method used to perform all the requested operation and output the result
	 * 
	 * @param data A list containing objects of type MonitorData consisting in all the activities stored individually
	 */
	public static void Output(List<MonitorData> data) {
		System.out.println("Number of total days observed: " + MonitorData.getNumberOfDays(data));
		
		System.out.println("\nActivity counts : ");
		
		Map<String, Integer> activityCount = MonitorData.getActivityCounter();
		activityCount.keySet().stream().forEach(item -> {
			System.out.println(padLeftSpaces(item) + activityCount.get(item) + ", Duration : " + activityTimeCounter.get(item));
			// System.out.println("\t" + item + " --> " + activityCount.get(item));
		});
		
		System.out.println("\nDaily activity counts");
		Map<Integer, Map<String, Integer>> dailyActivityCount = MonitorData.getDailyActivityCounter();

		dailyActivityCount.keySet().stream().forEach(day -> {
			System.out.print("\tDay : " + day + "\n");
			activityCounter = dailyActivityCount.get(day);
			activityCounter.keySet().stream().forEach(activity -> {
				System.out.print(padLeftSpaces(activity) + activityCounter.get(activity));
				//switch to println to see them one under another.
			});
			System.out.println();
		});
		
		// used to filter out activites that have overall duration less than 10 hours.
		activityTimeCounter = activityTimeCounter.entrySet()
				.stream()
				.filter(activity -> activity.getValue().toHours() >= 10)
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		System.out.println("\nActivites that take more than 10 hours : ");
		activityCount.keySet().stream().forEach(item -> {
			if(activityTimeCounter.get(item) != null) {
				System.out.println(padLeftSpaces(item) + activityCount.get(item) + ", Duration : " + activityTimeCounter.get(item));
			}
		});
		
		
		System.out.println("\nActivies whose 90% sample data took more than 5 minutes : ");
		MonitorData.getActivityTimeSample().entrySet().stream()
			.forEach(value -> {
				n = value.getValue().size();
				List<Duration> temp  = value.getValue().stream().filter(dur -> dur.toMinutes() >= 5).collect(Collectors.toList());
				k = temp.size();
				value.getValue().removeIf(x->(double)k/n<0.9);
				if(value.getValue().size() > 0) {
					System.out.println(value.getKey() + "  " + value.getValue());
				}
				});
	}

	/**
	 * 
	 * Method used to add left padding to a string
	 * 
	 * @param str the input string
	 * @return the formated string
	 */
	private static String padLeftSpaces(String str) {
		String data = "\t";
		data = data + str.trim();
		while (data.length() < 15) {
			data = data + " ";
		}
		data = data + "--> ";
		return data;
	}
}
